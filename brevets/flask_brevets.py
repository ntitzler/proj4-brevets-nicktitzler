"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

#logging.basicConfig(format='%(levelname)s:%(message)2',  level=logging.INFO) 
#log = logging.getLogger(__name__)
app.logger.setLevel(logging.DEBUG)

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    trueDistance = request.args.get('trueDistance',type=int)
    app.logger.debug("trueDistance={}".format(trueDistance))

    startTime = request.args.get('startTime',type=str)
    date = request.args.get('date',type=str)
    app.logger.debug("startTime={}".format(startTime))
    app.logger.debug("date={}".format(date))

     #"2013-05-05T12:30:45+00:00"
    inputStr = date + "T" + startTime + "+" + "00:00"
    #print("inputStr: ",inputStr)
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    

    # I changed the code here to shift the time by 8 hours because without it 
    # moment.js would misformat the string. This bug was encountered on line 149,150 in calc.html
    open_time = acp_times.open_time(km, trueDistance, inputStr)

    if (open_time == 0):
        error = "error"
        result = {"error": error}
        return flask.jsonify(result = result)
    
    dt = open_time.split("T")

    date = dt[0].split("-")
    zone = dt[1].split("+")[1]
    time = dt[1].split("+")[0]
    time = time.split(":")

    start_time2 = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    start_time2 = start_time2.shift(hours = +8)
    open_time = start_time2.isoformat()

    close_time = acp_times.close_time(km, trueDistance, inputStr)
    dt = close_time.split("T")
    date = dt[0].split("-")
    zone = dt[1].split("+")[1]
    time = dt[1].split("+")[0]
    time = time.split(":")

    close_time2 = arrow.Arrow(int(date[0]), int(date[1]), int(date[2]), int(time[0]), int(time[1]), int(time[2]))
    close_time2 = close_time2.shift(hours = +8)
    close_time = close_time2.isoformat()


    result = {"open": open_time, "close": close_time}
    
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
